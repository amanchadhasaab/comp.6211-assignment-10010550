﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JACOB1
{
    class Program
    {
        static void Main(string[] args)
        {
            string menuNum = "0";

            do
            {
                Console.Clear();

                // This is Menu
                Console.WriteLine(" -------------1. MyQueue Excersice----------------");
                Console.WriteLine(" -------------2. Palindorme-----------------------");
                Console.WriteLine(" -------------3. Convert infix to postfix---------");
                Console.WriteLine(" -------------4. Evaluate postfix-----------------");
                Console.WriteLine(" -------------5. Exit-----------------------------");

                // Line for choosing one selction
                Console.Write(" Select one of the menu : ");
                menuNum = Console.ReadLine();

                switch (menuNum)
                {
                    case "1": // refers to Question 1 selection - MyQueue
                        MyQueue myQueue = new MyQueue();
                        Console.ReadLine();
                        break;

                    case "2": // refers to Question 2 selection - Palindrome
                        Palindrome();
                        Console.ReadLine();
                        break;

                    case "3": // refers to Question 3 selection - InfixToPostfixConvertor
                        InfixToPostfixConverter infixToPostfixConverter = new InfixToPostfixConverter();
                        Console.ReadKey();
                        break;

                    case "4": // refers to Question 4 selection - Evaluating Post Fix
                        EvaluatePostfix evaluatePostfix = new EvaluatePostfix();
                        Console.ReadKey();
                        break;

                    case "5":     return; // commmand line to return back

                    default:    break;
                }
            } while (menuNum != "0");
        }

        static void Palindrome() // making input for palindrome
        {
            Stack<char> cstack = new Stack<char>();

            Console.Write("Type in a string : ");

            string input = Console.ReadLine();


            var inputToUpper = input.ToUpper(); //assuming case senstivity is not to be considered 

            foreach (char c in inputToUpper)
            {
                cstack.Push(c);
            }

            bool isPalindrome = true;
            var noOfItems = cstack.Count;

            for (int i = 0; i < noOfItems; i++)
            {
                if (inputToUpper[i] != cstack.Pop())
                {
                    isPalindrome = false; break;
                }
            }

            if (isPalindrome)
            {
                Console.WriteLine("Palindrome");
            }
            else
            {
                Console.WriteLine("Non Palindrome");
            }
        }

    }
}

