﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JACOB1
{

    public class MyQueue
    {
        Queue queue;

        public MyQueue()
        {
            queue = new Queue();
            showMenu();
        }

        public void showMenu()
        {
            string menuNum;

            do
            {
                Console.Clear();
                Console.WriteLine("--------------- 1. Enqueue---------------");
                Console.WriteLine("--------------- 2. Dequeue---------------");
                Console.WriteLine("--------------- 3. Contains---------------");
                Console.WriteLine("--------------- 4. To Array---------------");
                Console.WriteLine("--------------- 5. Print Queue---------------");
                Console.WriteLine("--------------- 6. exit---------------");
                Console.WriteLine();

                Console.Write(" Select one of the menu : ");

                menuNum = Console.ReadLine();

                switch (menuNum)
                {
                    case "1":
                        Console.Write("Type in a string to enqueue: ");
                        Enqueue(Console.ReadLine());

                        Console.WriteLine("Queue contains :");
                        PrintQueue();
                        break;

                    case "2":
                        Dequeue();
                        Console.WriteLine("Dequeued!");

                        Console.WriteLine("Queue contains :");
                        PrintQueue();
                        break;

                    case "3":
                        Console.Write("Type in a string to check if it is contained : ");
                        string str = Console.ReadLine();
                        bool isContained = Contains(str);
                        if(isContained)
                        {
                            Console.WriteLine("Yes, {0} is contained.", str);
                        }
                        else
                        {
                            Console.WriteLine("No, {0} is not contained.", str);
                        }
                        break;

                    case "4":
                        object[] arr = ToArray();
                        Console.WriteLine("You converted the queue to a array.");
                        break;

                    case "5":
                        Console.WriteLine("Queue contains :");
                        PrintQueue();
                        break;

                    case "6": return;

                    default: break;
                }

                Console.ReadLine();

            } while (menuNum != "0");
        }

        public void PrintQueue()
        {
            foreach (object obj in queue)
            {
                Console.WriteLine(obj);
            }
        }

        public void Enqueue(object obj)
        {
            queue.Enqueue(obj);
        }

        public void Dequeue()
        {
            queue.Dequeue();
        }

        public bool Contains(object obj)
        {
            return queue.Contains(obj);
        }

        public object[] ToArray()
        {
            return queue.ToArray();
        }
    }
}