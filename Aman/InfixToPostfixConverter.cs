﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JACOB1
{

    public class InfixToPostfixConverter
    {
        public InfixToPostfixConverter()
        {
            Console.WriteLine("Enter a infix String (+,-,*,/,%,^ and number):");
            String str = Console.ReadLine();

            char[] infix = str.ToCharArray();
            if (!isValid(infix))
            {
                Console.WriteLine("You have entered Invalid Character");
            }
            else
            {
                ConvertToPostfix(infix);
            }
            Console.ReadKey();
        }

        public Boolean isValid(char[] infix)
        {
            for (int i = 0; i < infix.Length; i++)
            {
                char ch = infix[i];
                if (!isNum(ch) && !IsOperator(ch) && ch != '(' && ch != ')')
                {
                    Console.WriteLine(ch + " is not allowed");
                    return false;
                }
            }
            return true;
        }

        public bool IsOperator(char ch)
        {
            return (ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == '%' || ch == '^');
        }

        public void print(char[] postfix, int length)
        {
            for (int i = 0; i <= length; i++)
            {
                Console.Write(postfix[i]);
            }
        }

        public void ConvertToPostfix(char[] infix)
        {
            Stack stack = new Stack(50);
            char[] postfix = new char[50];
            int j = 0;
            Console.WriteLine("Scan\t\tStack\t\tPostfix");
            for (int i = 0; i < infix.Length; i++)
            {
                if (isNum(infix[i]))
                {
                    postfix[j] = infix[i];
                    j++;
                }
                else
                {
                    if (infix[i] == ')')
                    {
                        while (stack.peep() != '(')
                        {
                            postfix[j] = stack.pop();
                            j++;
                        }
                        stack.pop();
                    }
                    else if (infix[i] == '(')
                    {
                        stack.push(infix[i]);
                    }
                    else if ((Precedence(stack.peep())) < Precedence(infix[i]))
                    {
                        stack.push(infix[i]);
                    }
                    else if ((Precedence(stack.peep())) >= Precedence(infix[i]))
                    {
                        char c = stack.pop();
                        postfix[j] = c;
                        j++;
                        stack.push(infix[i]);
                    }
                }
                Console.Write(infix[i] + "\t\t");
                stack.print();
                print(postfix, j - 1);
                //Console.WriteLine(postfix + "" + (j - 1));
                Console.Write("\n");
            }
            while (!stack.isEmpty())
            {
                postfix[j] = stack.pop();
                j++;
                Console.Write("\t\t");
                stack.print();
                print(postfix, j - 1);
                //Console.WriteLine(postfix + "" + (j - 1));
                Console.Write("\n");
            }
            Console.WriteLine("Postfix String:");
            for (int i = 0; i < postfix.Length; i++)
            {
                Console.Write(postfix[i]);
            }
        }
        static int Precedence(char ch)
        {
            if (ch == '(' || ch == ')')
                return 1;
            else if (ch == '+' || ch == '-')
                return 2;
            else if (ch == '*' || ch == '/' || ch == '%')
                return 3;
            else if (ch == '^')
                return 4;
            return 0;
        }
        static Boolean isNum(char ch)
        {
            if (ch >= '0' && ch <= '9')
            {
                return true;
            }
            
            return false;
        }
    }


    public class Stack
    {
        char[] array = null;
        int top = -1;

        public Stack(int size)
        {
            array = new char[size];
        }

        public Boolean isEmpty()
        {
            if (top == -1)
            {
                return true;
            }
            return false;
        }

        public char peep()
        {
            if (!isEmpty())
            {
                return array[top];
            }
            return 'Z';
        }

        public void push(char ch)
        {
            top++;
            array[top] = ch;
        }

        public char pop()
        {
            if (!isEmpty())
            {
                char ch = array[top];
                top--;
                return ch;
            }
            return 'Z';
        }

        public void print()
        {
            for (int i = 0; i <= top; i++)
            {
                Console.Write(array[i]);
            }
            Console.Write("\t\t");
        }

    }
}