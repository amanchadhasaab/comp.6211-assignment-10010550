﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JACOB1
{

    public class EvaluatePostfix
    {
        public delegate double BinOp(double a, double b);
        static double add(double a, double b) { return a + b; }
        static double sub(double a, double b) { return a - b; }
        static double mul(double a, double b) { return a * b; }
        static double div(double a, double b) { return a / b; }
        static double rem(double a, double b) { return a % b; }
        static double pow(double a, double b) { return Math.Pow(a, b); }

        public EvaluatePostfix()
        {
            
            Console.Write("Type in postfix expression : ");
            string exp = Console.ReadLine();

            Console.WriteLine("expression '{0}' result is '{1}'", exp, EvaluatePostfixExpression(exp));
            Console.ReadKey();
        }

        static double EvaluatePostfixExpression(string s)
        {
            Dictionary<string, BinOp> oper = new Dictionary<string, BinOp>();
            List<double> stack = new List<double>();

            oper["+"] = add;
            oper["-"] = sub;
            oper["*"] = mul;
            oper["/"] = div;
            oper["%"] = rem;
            oper["^"] = pow;

            Regex rxnum = new Regex(@"\G([0-9])");
            Regex rxop = new Regex(@"\G(\+|\-|\*|\/|\%|\^)");
            int start = 0;
            do
            {
                Match m = rxnum.Match(s, start);
                if (m.Success)
                {
                    stack.Add(Int32.Parse(m.ToString()));
                }
                else
                {
                    m = rxop.Match(s, start);
                    if (m.Success)
                    {
                        int c = stack.Count;
                        if (c < 2) throw new Exception("Invalid expression: out of stack.");
                        stack[c - 2] = oper[m.ToString()](stack[c - 2], stack[c - 1]);
                        stack.RemoveAt(c - 1);
                    }
                    else break;
                }
                start = start + m.Length;
            } while (true);

            if (stack.Count != 1) throw new Exception("Invalid expression: more than one result on stack.");
            if (start != s.Length) throw new Exception("Invalid expression: unrecognized token.");

            return stack[0];
        }
    }
}